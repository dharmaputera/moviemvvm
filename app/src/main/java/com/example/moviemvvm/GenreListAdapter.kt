package com.example.moviemvvm

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.genre_item.view.*

class GenreListAdapter (private val genres: List<Genre>, private val context: Context) : RecyclerView.Adapter<GenreHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreHolder {
        return GenreHolder(LayoutInflater.from(parent.context).inflate(R.layout.genre_item, parent, false))
    }

    override fun getItemCount(): Int {
        return genres.size
    }

    override fun onBindViewHolder(holder: GenreHolder, position: Int) {
        holder.bindGenre(genres[position], context)
    }
}

class GenreHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val genreName = view.txtGenreName

    fun bindGenre(genre: Genre, context: Context) {
        genreName.text = genre.name
        itemView.setOnClickListener {
            Log.e("GenreList", "Clicked "+ genre.name)
            val intent = Intent(context, ListMovieActivity::class.java)
            intent.putExtra("genreId", genre.id)
            context.startActivity(intent)
        }
    }
}