package com.example.moviemvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable

class ReviewPagedListRepository (private val apiService : TMDBInterface, private val movieId: Int) {
    lateinit var reviewPagedList: LiveData<PagedList<ResultX>>
    lateinit var reviewDataSourceFactory: ReviewDataSourceFactory

    fun fetchLiveReviewPagedList (compositeDisposable: CompositeDisposable) : LiveData<PagedList<ResultX>> {
        reviewDataSourceFactory = ReviewDataSourceFactory(apiService, compositeDisposable, movieId)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(POST_PER_PAGE)
            .build()

        reviewPagedList = LivePagedListBuilder(reviewDataSourceFactory, config).build()

        return reviewPagedList
    }

    fun getNetworkState(): LiveData<NetworkState> {
        return Transformations.switchMap<ReviewDataSource, NetworkState>(
            reviewDataSourceFactory.reviewLiveDataSource, ReviewDataSource::networkState)
    }
}