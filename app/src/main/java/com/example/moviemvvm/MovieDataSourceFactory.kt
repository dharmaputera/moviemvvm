package com.example.moviemvvm

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable

class MovieDataSourceFactory(private val apiService : TMDBInterface, private val compositeDisposable: CompositeDisposable, private val genreId : Int)
    : DataSource.Factory<Int, Movie>() {

    val moviesLiveDataSource =  MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val movieDataSource = MovieDataSource(apiService,compositeDisposable, genreId)

        moviesLiveDataSource.postValue(movieDataSource)
        return movieDataSource
    }
}