package com.example.moviemvvm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GenreNetworkDataSource (private val apiService : TMDBInterface, private val compositeDisposable: CompositeDisposable) {
    private val _networkState  = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState                   //with this get, no need to implement get function to get networkSate

    private val _downloadedGenreResponse =  MutableLiveData<GenreResponse>()
    val downloadedGenreResponse: LiveData<GenreResponse>
        get() = _downloadedGenreResponse

    fun fetchGenre(language: String) {

        _networkState.postValue(NetworkState.LOADING)


        try {
            compositeDisposable.add(
                apiService.getGenre(language)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _downloadedGenreResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("GenreDataSource", it.message)
                        }
                    )
            )

        }

        catch (e: Exception){
            Log.e("MovieDetailsDataSource",e.message)
        }


    }
}