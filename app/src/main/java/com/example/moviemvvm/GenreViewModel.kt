package com.example.moviemvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

class GenreViewModel (private val genreRepository: GenreRepository , language: String)  : ViewModel(){
    private val compositeDisposable = CompositeDisposable()

    val  genres : LiveData<GenreResponse> by lazy {
        genreRepository.fetchGenre(compositeDisposable,language)
    }

    val networkState : LiveData<NetworkState> by lazy {
        genreRepository.getGenreNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return genres.value?.genres?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}