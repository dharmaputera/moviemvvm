package com.example.moviemvvm

data class GenreResponse(
    val genres: List<Genre>
)