package com.example.moviemvvm

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable

class ReviewDataSourceFactory(private val apiService : TMDBInterface, private val compositeDisposable: CompositeDisposable, private val movieId : Int)
    : DataSource.Factory<Int, ResultX>()  {

    val reviewLiveDataSource =  MutableLiveData<ReviewDataSource>()

    override fun create(): DataSource<Int, ResultX> {
        val reviewDataSource = ReviewDataSource(apiService,compositeDisposable, movieId)

        reviewLiveDataSource.postValue(reviewDataSource)
        return reviewDataSource
    }
}