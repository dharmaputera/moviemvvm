package com.example.moviemvvm

import androidx.lifecycle.LiveData
import io.reactivex.disposables.CompositeDisposable

class ReviewRepository (private val apiService : TMDBInterface) {
    lateinit var reviewNetworkDataSource: ReviewNetworkDataSource

    fun fetchReview (compositeDisposable: CompositeDisposable, movieId:Int, language: String) : LiveData<ReviewResponse> {

        reviewNetworkDataSource = ReviewNetworkDataSource(apiService,compositeDisposable)
        reviewNetworkDataSource.fetchReview(movieId, language)

        return reviewNetworkDataSource.downloadedReviewResponse

    }

    fun getReviewNetworkState(): LiveData<NetworkState> {
        return reviewNetworkDataSource.networkState
    }
}