package com.example.moviemvvm

import androidx.lifecycle.LiveData
import io.reactivex.disposables.CompositeDisposable

class GenreRepository (private val apiService : TMDBInterface) {
    lateinit var genreNetworkDataSource: GenreNetworkDataSource

    fun fetchGenre (compositeDisposable: CompositeDisposable, language: String) : LiveData<GenreResponse> {

        genreNetworkDataSource = GenreNetworkDataSource(apiService,compositeDisposable)
        genreNetworkDataSource.fetchGenre(language)

        return genreNetworkDataSource.downloadedGenreResponse

    }

    fun getGenreNetworkState(): LiveData<NetworkState> {
        return genreNetworkDataSource.networkState
    }
}