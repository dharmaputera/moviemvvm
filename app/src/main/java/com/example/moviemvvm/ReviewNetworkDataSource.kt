package com.example.moviemvvm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ReviewNetworkDataSource (private val apiService : TMDBInterface, private val compositeDisposable: CompositeDisposable)  {
    private var page = FIRST_PAGE
    private val _networkState  = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState                   //with this get, no need to implement get function to get networkSate

    private val _downloadedGenreResponse =  MutableLiveData<ReviewResponse>()
    val downloadedReviewResponse: LiveData<ReviewResponse>
        get() = _downloadedGenreResponse

    fun fetchReview(movieId: Int, language: String) {

        _networkState.postValue(NetworkState.LOADING)


        try {
            compositeDisposable.add(
                apiService.getReview(movieId, page)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _downloadedGenreResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("GenreDataSource", it.message)
                        }
                    )
            )

        }

        catch (e: Exception){
            Log.e("MovieDetailsDataSource",e.message)
        }


    }
}