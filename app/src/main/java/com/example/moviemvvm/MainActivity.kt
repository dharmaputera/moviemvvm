package com.example.moviemvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.moviemvvm.R
import kotlinx.android.synthetic.main.activity_main.*
import java.text.NumberFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: GenreViewModel
    private lateinit var genreRepository: GenreRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val apiService : TMDBInterface = TMDBClient.getClient()
        genreRepository = GenreRepository(apiService)

        viewModel = getViewModel("en-US")

        viewModel.genres.observe(this, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(this, Observer {
            genreLoading.visibility = if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
            responseError.visibility = if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE

        })
    }

    fun bindUI( it: GenreResponse){
        Log.e("Genre", it.toString())
        val genreAdapter = GenreListAdapter(it.genres, this)
        rvGenre.layoutManager = LinearLayoutManager(this)
        rvGenre.adapter = genreAdapter

    }

    private fun getViewModel(language:String): GenreViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return GenreViewModel(genreRepository,language) as T
            }
        })[GenreViewModel::class.java]
    }
}