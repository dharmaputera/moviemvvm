package com.example.moviemvvm

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_movie.*
import kotlinx.android.synthetic.main.activity_main.*

class ReviewActivity : AppCompatActivity() {

    private lateinit var viewModel: ListReviewViewModel

    lateinit var reviewRepository: ReviewPagedListRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val movieId: Int = intent.getIntExtra("movieId",1)
        val apiService : TMDBInterface = TMDBClient.getClient()
        reviewRepository = ReviewPagedListRepository(apiService, movieId)

        viewModel = getViewModel()

        val reviewAdapter = ListReviewAdapter(this)

        val linearLayoutManager = LinearLayoutManager(this)

        rvGenre.layoutManager = linearLayoutManager
        rvGenre.setHasFixedSize(true)
        rvGenre.adapter = reviewAdapter

        viewModel.reviewPagedList.observe(this, androidx.lifecycle.Observer {
            reviewAdapter.submitList(it)
        })

        viewModel.networkState.observe(this, androidx.lifecycle.Observer {
            genreLoading.visibility = if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
            responseError.visibility = if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE

            if (!viewModel.listIsEmpty()) {
                reviewAdapter.setNetworkState(it)
            }
        })

    }

    private fun getViewModel(): ListReviewViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return ListReviewViewModel(reviewRepository) as T
            }
        })[ListReviewViewModel::class.java]
    }
}