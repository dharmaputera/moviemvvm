package com.example.moviemvvm

data class Genre(
    val id: Int,
    val name: String
)