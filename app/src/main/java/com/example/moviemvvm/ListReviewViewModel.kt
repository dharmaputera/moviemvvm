package com.example.moviemvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable

class ListReviewViewModel(private val reviewRepository: ReviewPagedListRepository)  : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    val  reviewPagedList : LiveData<PagedList<ResultX>> by lazy {
        reviewRepository.fetchLiveReviewPagedList(compositeDisposable)
    }

    val  networkState : LiveData<NetworkState> by lazy {
        reviewRepository.getNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return reviewPagedList.value?.isEmpty() ?: true
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}