package com.example.moviemvvm

data class ResultX(
    val author: String,
    val content: String,
    val id: String,
    val url: String
)