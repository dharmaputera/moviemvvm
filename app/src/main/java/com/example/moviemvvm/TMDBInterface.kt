package com.example.moviemvvm

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBInterface {
    @GET("genre/movie/list")
    fun getGenre(@Query("language") language: String): Single<GenreResponse>

    @GET("discover/movie")
    fun getMovieByGenre(
        @Query("page") page: Int,
        @Query("with_genres") genre: Int
    ): Single<ListMovie>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") id: Int,
        @Query("append_to_response") appended: String
    ): Single<MovieDetails>

    @GET("movie/{movie_id}/reviews")
    fun getReview(
        @Path("movie_id") id: Int,
        @Query("page") page: Int
    ): Single<ReviewResponse>

}